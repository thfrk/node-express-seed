var express= require('express')
var expressBodyParser= require('body-parser')

var app= express() // экземпляр приложения



app.use(express.static(__dirname+'/public'))

// GET http://localhost:8000/hello
app.get('/hello', function (req, res, next) { // обработчик запроса
  res.send('hello world')
})



// API блога, например

var blog= {
    posts: [],
    postId: 1,
    postsIdx: {}
}

// список постов
app.get('/blog/posts', function (req, res, next) {
	res.send(blog.posts)
	
})

// добавить пост в список
app.post('/blog/posts', expressBodyParser.json(), function (req, res, next) {
    var post= req.body
    post.id= blog.postId++

    blog.posts.push(
        blog.postsIdx[post.id]= post
    )

    console.log('post created', post)
    res.status(201).send(post)
})

// обновить пост
app.put('/blog/posts/:postId', expressBodyParser.json(), function (req, res, next) {

    var post= req.body
    post.id= req.params.postId

    if (blog.postsIdx[post.id]) {
        var oldPost= blog.postsIdx[post.id]
        var oldPostIndex= blog.posts.indexOf(oldPost)
        blog.posts.splice(oldPostIndex, 1, post)
        blog.postsIdx[post.id]= post
    }

    console.log('post updated', post)
    res.status(202).send(post)
})

// удалить пост
app.delete('/blog/posts/:postId', function (req, res, next) {
    var postId= req.params.postId

    if (blog.postsIdx[postId]) {
        var oldPost= blog.postsIdx[postId]
        var oldPostIndex= blog.posts.indexOf(oldPost)
        blog.posts.splice(oldPostIndex, 1)
        delete blog.postsIdx[postId]
    }

    console.log('post deleted', oldPost)
    res.status(204).send(oldPost)
})



var port= 8000 // http://localhost:8000

app.listen(port, function () {
    console.log('application listening on port %d', port)
})
