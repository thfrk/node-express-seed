angular.module('myApp',[])
/*.service('postService', function ($http, $q) {
	this.
})*/
.factory('sendPost', function ($http, $q) {
	return function (post) {
		var deffered = $q.defer();
		$http.post('/blog/posts', post).then(function (res) {
			 deffered.resolve(res.data)
		},function(err){
			deffered.reject(Error('cannot set post'))
		})
		return deffered.promise;
	}
})
.factory('deletePost', function ($http, $q) {
	return function (post) {
		var deffered = $q.defer();
		$http.delete('/blog/posts/'+post.id).then(
			function (res) {
				deffered.resolve(res.data)
			},
			function(err) {
				deffered.reject(Error('cannot set post'))
		})
		return deffered.promise;
	}
})
.factory('getPosts', function ($http, $q) {
	return function(){
		var deffered = $q.defer();
		$http.get('/blog/posts').then(
			function (res) {
				deffered.resolve(res.data)
			},
			function(err) {
				deffered.reject(Error('cannot get posts'))
		})
		return deffered.promise;
	}
})
.factory('putPost', function ($http, $q) {
	return function(post){
		var deffered = $q.defer();
		$http.put('/blog/posts/'+post.id, post).then(function (res) {
			deffered.resolve(res.data)
		},function(err){
			deffered.reject(Error('cannot put post'))
		})
	}
})
.service('postService', function(sendPost, deletePost, putPost, getPosts) {
	this.sendPost = sendPost;
	this.deletePost = deletePost;
	this.putPost = putPost;
	this.getPosts = getPosts;
})
.controller('newPost',function ($scope, postService){
		$scope.isDisabled = false
	$scope.sendPost = function (post) {
		$scope.isDisabled = true
		postService.sendPost(post)
		.then(function(){
			post.body = post.head = ''
		})
		.catch(function(err){
			console.log(err)
		})
		.finally(function(){
			$scope.isDisabled = false
		});
	}
})
.controller('allPosts',function ($scope,$interval,$http, postService) {
	$scope.posts = [];
	$scope.isDelete = false;
	$scope.hide = false;
	$scope.isChange = false;
	$scope.savePost = function (post) {
		$scope.oldVersionPost = angular.copy(post);
	}
	$scope.refreshPost = postService.putPost;
	$scope.deleteThisPost = postService.deletePost;

	$interval(function () {
		postService.getPosts()
		.then(
			function (res) {
				$scope.posts = ($scope.posts.length != res.length) ? res : $scope.posts;
			}
		)
		.catch(
			function (err) {
				console.log(err);
			}
		)
	}, 5000)
})
.controller('СhangePostCtrl',function($scope){
	$scope.oldVersionPost = {};
})